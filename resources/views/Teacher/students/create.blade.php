@extends('main')

@section('content')
 
 <div class="row">
      <div class="col-md-6 col-md-offset-3">
	        <div class="panel panel-default">
              <div class="panel-heading">
	                <h4>Creation d'un compte etudiant</h4>
	            </div>
	            <div class="panel-body">
	                <form action="{{route('students.store')}}" method="post">
	                {{csrf_field()}} 

	               
	              

	                <div class="form-group">
	                    <label for="CNE">CNE :</label>
	                    <input class="form-control" name="CNE" placeholder="CNE" type="text"  />
	                   
	                </div>
					
					<div class="form-group">
	                    <label for="fname">Prenom :</label>
	                    <input class="form-control" name="fname" placeholder="Prenom" type="text" />
	                   
	                </div>

	                <div class="form-group">
	                    <label for="lname">Nom :</label>
	                    <input class="form-control" name="lname" placeholder="Nom" type="text" />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="birth_date">Date de Naissance :</label>
	                    <input class="form-control" name="birth_date" placeholder="Date Naissance" type="date" />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="birth_city">Ville de Naissance :</label>
	                    <input class="form-control" name="birth_city" placeholder="Ville Naissance" type="text" />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="adress">Adresse :</label>
	                    <input class="form-control" name="adress" placeholder="Adresse" type="text"  />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="city">Ville :</label>
	                    <input class="form-control" name="city" placeholder="Ville" type="text" />
	                    
	                </div>

	                 <div class="form-group">
		                <div class="row">
		                    <div class="col-md-12">
		                        <label for="photo" class="control-label">Photo</label>
		                    </div>
		                </div>
		            </div>

		            <div class="form-group">
		                <div class="row">
		                    <div class="col-md-12">
		                    	<input type="file" name="image" id="image" class="form-control">
		                    </div>
		                </div>
		            </div>
	                
	                <div class="form-group">
	                    <label for="email">Email :</label>
	                    <input class="form-control" name="email" placeholder="Email" type="text"  />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="phone">Telephone :</label>
	                    <input class="form-control" name="phone" placeholder="Telephone" type="text"  />
	                   
	                </div>

	                <div class="form-group">
	                    <input name="submit" type="submit" class="btn btn-primary" value="Inserer" />
	                    <input name="cancel" type="reset" class="btn btn-default" value="Cancel" />
	                </div>
	                
	            </form>
	            </div>
	        </div>
	        </div>
	        </div>
@endsection