@extends('main')

@section('content')


<div class="row">
	
		<div class="col-md-6 col-md-offset-2">

			<h3>Liste des etudiants</h3>
			<table class="table table-striped">
		    	<thead>
		    		<tr>
		        		<th>Photo</th>
		        		<th>CNE</th>
		        		<th>Nom</th>
		        		<th>Prenom</th>
		        		<th>Date naissance</th>
		        		<th>ville naissance</th>
		        		<th>Adresse</th>
		        		<th>Ville</th>
		        		<th>Email</th>
		        		<th>Telephone</th>
		        		<th class="ico">Affecter un cours</th>
		        		<th class="ico">View</th>
		        		<th class="ico">Edit/Del</th>
		      		</tr>
		    	</thead>
		    	<tbody>
					@foreach($students as $student)
							<tr>
								<td> image </td>
						        <td>{{$student->CNE}}</td>
						        <td>{{$student->fname}}</td>
						        <td>{{$student->lname}}</td>
						        <td>{{$student->birth_date}}</td>
						        <td> {{$student->birth_city}}</td>
						        <td>{{$student->adress}}</td>
						        <td>{{$student->city}}</td>
						        <td>{{$student->email}}</td>
						        <td>{{$student->phone}}</td>
						       <td class="ico"> <button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
									</button></td>
						 <td class="ico">
					        	
							        <button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
									</button>
									
							</td>
						        
									
								</td>
						        <td class="ico">
					        		
							        <button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									</button>
									
							</tr>
					@endforeach
					
				</tbody>
	  		</table>
			<br>
			<center>
	  			
	  			
				<button type="button" class="btn btn-default btn"/>
		        	<a href="#">
				    <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
				    Export as XLS</a>
				</button>
			</center>	
			<br><br>
@endsection