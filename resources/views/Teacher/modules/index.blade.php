@extends('main')

@section('content')

<div class="row">
		<div class="col-md-6 col-md-offset-3">

		<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th><h3> Les modules </h3> </th>
    </tr>
  </thead>
  	<tbody>
  	

		 	@foreach($modules as $module)
		

	    <tr>
	      <th scope="row">{{ $count ++}}</th>
	      <td>{{$module->entitled_module}}</td>
	 
	    </tr>
		
					
		@endforeach
		</tbody>
		</table>
		
	</div>
	
</div>

@endsection