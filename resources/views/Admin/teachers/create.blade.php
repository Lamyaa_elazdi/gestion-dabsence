@extends('main')

@section('stylesheet')
{!! Html::style('css/select2.min.css') !!}


@section('content')


 <div class="row">
      <div class="col-md-6 col-md-offset-3">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <h4>Creation d'un compte enseignant</h4>
	            </div>
	            <div class="panel-body">
	           
	              <form action="{{route('teachers.store')}}" method="POST">
		{{csrf_field()}}  
	              
	           

	              
					
					<div class="form-group">
	                    <label for="name">Prenom :</label>
	                    <input class="form-control" name="fname" 
	                  id="irst_name_teacher"  placeholder="Prenom" type="text" />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="name">Nom :</label>
	                    <input class="form-control" name="lname" 
	                     id="last_name_teacher" placeholder="Nom" type="text" />
	                    
	                </div>

	                <div class="form-group">
	                    <label for="name">Adresse :</label>
	                    <input class="form-control" name="adress" id="adress" placeholder="Adresse" type="text"/>
	                   
	                </div>

	                <div class="form-group">
	                    <label for="name">Ville :</label>
	                    <input class="form-control" name="city" id="city" placeholder="Ville" type="text"/>
	                    
	                </div>

	                <div class="form-group row col-xs-9">
	                 
						<label name="modules">Module:</label>
							
								
							<select name="modules[]" class="form-control select2-multi" multiple="multiple">
					@foreach($modules as $module)
					<option value="{{$module->id}}">{{$module->entitled_module}}</option>
					@endforeach
				</select>

					<a href="{{route('modules.create')}}" class="btn btn-primary">Créer</a>
							
				
				</div>

			
	                <div class="form-group">
		                <div class="row">
		                    <div class="col-md-12">
		                        <label for="name" class="control-label">Photo</label>
		                       
		                      </div>


		                </div>
		            </div>
  <div class="form-group">
		                <div class="row">
		                    <div class="col-md-12">
		                    	 <input type="file" name="image" id="image" class="form-control">
		                    </div>
		                </div>
		            </div>
	                
	                <div class="form-group">
	                    <label for="email">Email :</label>
	                    <input class="form-control" name="email" id="email" placeholder="Email" type="text"/>
	                    
	                </div>

	                <div class="form-group">
	                    <label for="name">Telephone :</label>
	                    <input class="form-control" name="phone" id="phone_number" placeholder="Telephone" type="text"/>
	                   
	                </div>

	                <div class="form-group">
	                    <input name="submit" type="submit" class="btn btn-primary" value="Inserer" />
	                    <input name="cancel" type="reset" class="btn btn-default" value="Cancel" />
	                </div>
	                
	            </div>
	        </div>
	  
</form>
</div>
</div>
@endsection
@section('script')
{!! Html::script('js/select2.min.js') !!}

<script type="text/javascript">
$('.select2-multi').select2();
</script>
@endsection