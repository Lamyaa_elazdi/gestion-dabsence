@extends('main')


@section('stylesheet')
{!! Html::style('css/custom.min.css') !!}

@section('content')


	<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            {{$teacher->fname}} {{$teacher->lname}}</h4>
                        <small><cite title="adresse"> {{$teacher->adress}}<i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon glyphicon-envelope"></i>{{$teacher->email}}
                            <br />
                           
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>{{$teacher->phone}}</p>
                       
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Liste des enseignants</h3>
     <div class="col-md-6 col-md-offset-3">
			<table class="table table-striped table-inverse">
		    	<thead>
		    		<tr>
		        		<th>Les modules</th>
		        		<th>Les classes</th>

		      		</tr>
		    	</thead>
		    	<tbody>
		    	<tr>
		    		<td>modules</td>
		    		<td>classes</td>
		    	</tr>
		    	</tbody>
		    	</table></div>
		    	</div>
</div>

@endsection