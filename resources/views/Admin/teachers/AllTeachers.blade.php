@extends('main')

@section('content')

  <div class="row">
      <div class="col-md-6 col-md-offset-3">
		<h3>Liste des enseignants</h3>
			<table class="table table-striped table-inverse">
		    	<thead>
		    		<tr>
		        		<th>Photo</th>
		        		<th>Nom</th>
		        		<th>Prenom</th>
		        		<th>Adresse</th>
		        		<th>Ville</th>
		        		<th>Email</th>
		        		<th>Telephone</th>
		        		<th class="ico">Affecter à un cours</th>
		        	
		        		<th class="ico">Consulter</th>
		        		<th class="ico">Modifier/Supprimer</th>
		      		</tr>
		    	</thead>
		    	<tbody>
					
						@foreach ($teachers as $teacher)
						<tr>
						<td>image</td>
						<td>{{$teacher->fname}}</td>
						<td>{{$teacher->lname}}</td>
						<td>{{$teacher->adress}}</td>
						<td>{{$teacher->city}}</td>
						<td>{{$teacher->email}}</td>
						<td>{{$teacher->phone}}</td>
						<td class="ico"> <button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
									</button></td>
										<td class="ico"> <button type="button" class="btn btn-default btn-sm">
							        	<a href="{{route('teachers.show',$teacher->id)}}">
									    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
									</button></td>
						 
						        
						        <td class="ico">
					        		
							        <button type="button" class="btn btn-default btn-sm">
							        	<a href="{{route('teachers.edit',$teacher->id)}}"/>
									    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
									    </a>
									</button>

									<form action="{{route('teachers.destroy',$teacher->id)}}" method="POST">
									{{csrf_field()}}
                            {{method_field('DELETE')}}

										<button type="submit" class="btn btn-default btn-sm">
							        	
									    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									    
									</button>
									</form>
										
									
								</td>	
									
							</tr>
					@endforeach
					
				</tbody>
	  		</table>
			<br>
			<center>
	  			
	  			
				<button type="button" class="btn btn-default btn"/>
		        	<a href="#">
				    <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
				    Export as XLS</a>
				</button>
			</center>	
			<br><br>
		</div>
		</div>

@endsection