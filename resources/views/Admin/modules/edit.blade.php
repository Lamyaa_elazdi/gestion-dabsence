@extends('main')

@section('content')



<div class="row">
	    <div class="col-md-6 col-md-offset-3">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <h4>Modification du nom du module</h4>
	            </div>
	            <div class="panel-body">
	               
	                  <form action="{{route('modules.update',$module->id)}}" method="POST">
		{{csrf_field()}}  
	    {{method_field('PUT')}}

	                <div class="form-group">
	                    <label for="name">Nom du Module :</label>
	                    <input class="form-control" name="mname" placeholder="Nom du Module" type="text" value="{{ $module->entitled_module }}" /> 
	                   
	                </div>
	                
	                <div class="form-group">
	                    <input name="submit" type="submit" class="btn btn-primary" value="Update" />
	                    <input name="cancel" type="reset" class="btn btn-default" value="Cancel" />
	                </div>
	           
	                </form>
	            </div>
	        </div>
	    </div>
	</div>

@endsection