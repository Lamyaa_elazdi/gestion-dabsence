@extends('main')

@section('content')


<div class="row">
	    <div class="col-md-6 col-md-offset-3">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <h4>Creation d'un Module</h4>
	            </div>
	            <div class="panel-body">
	           <form action="{{route('modules.store')}}" method="POST">
	           {{csrf_field()}}  
	           	

	           	<div class="form-group">
	                    <label for="name">Nom du Module :</label>
	                    <input class="form-control" name="module_name" placeholder="Nom du Module" type="text" />
	                   
	                </div>

	                <div class="form-group">
	                    <input name="submit" type="submit" class="btn btn-primary" value="Inserer" />
	                    <input name="cancel" type="reset" class="btn btn-default" value="Cancel" />
	                </div>
	           </form>
	           
	            </div>
	        </div>
	    </div>
	</div>
@endsection