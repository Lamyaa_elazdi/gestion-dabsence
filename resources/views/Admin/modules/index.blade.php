@extends('main')

@section('content')

<div class="row">
		<div class="col-md-6 col-md-offset-3">

					<h3>Liste des Modules</h3>
			<center>
			<table class="table table-striped">
		    	<thead>
		    		<tr>
		        		<th>ID</th>
		        		<th>Module</th>
		        		<th class="ico">Modifier/Supprimer</th>
		      		</tr>
		    	</thead>
		    	<tbody>
					 
						@foreach ($modules as $module)
						
							<tr>
						        <td> {{ $module->id }} </td>
						        <td>  {{ $module->entitled_module }} </td>
						        <td class="ico">
					        		
							        <button type="button" class="btn btn-default btn-sm">
							        	<a href="{{route('modules.edit', $module->id)}}">
									    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
									</button>
									<button type="button" class="btn btn-default btn-sm">
							        	<a href="#">
									    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									</button>
									
								</td>
						   </tr>
					@endforeach
					
				</tbody>
	  		</table>
 
		
	</div>
	
</div>

@endsection