@if (Session::has('success'))

	<div class="alert alert-success" role="alert">
		<strong>Success:</strong> {{Session::get('success')}}

	</div>

@endif

@if(count($errors)>0)

	@foreach($errors->all() as $error)

		<ul>
			
			<li class="alert alert-danger">

			{{$error}}

			</li>
		</ul>
		@endforeach

@endif