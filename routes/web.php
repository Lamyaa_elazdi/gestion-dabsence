<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>'auth'],function()
{
	Route::get('logout','Auth\LoginController@logout')->name('logout');
    Route::get('users/logout','Auth\LoginController@userLogout')->name('users.logout');

});


Auth::routes();
	
	


Route::prefix('admin')->group(function(){

 Route::get('/login','Auth\AdminLogingController@showLoginForm')->name('admin.login');
 Route::post('/login','Auth\AdminLogingController@login')->name('admin.login.submit');
 Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
 Route::get('/logout', 'Auth\AdminLogingController@logout')->name('admin.logout');

 	

});

Route::group([
		'as' => 'admin',
		'prefix' => 'admin',
		'namespace' => 'Admin'
	], function(){
		Route::resource('students','StudentController');
		Route::resource('teachers','TeacherController');
		Route::resource('modules','ModuleController');

			});

Route::group([
		'as' => 'teacher',
		'prefix' => 'teacher',
		'namespace' => 'Teacher'
	], function(){
		Route::get('index', [
			'as' =>'index',
			'uses' => 'HomeController@index'
			]);
		
		Route::resource('students','StudentController');
		Route::resource('modules','ModuleController');
	});

Route::get('/user/redirect', [
		'as'   => 'user.redirect',
		function(){

			switch (Auth::user()->who) {
				case 'teacher':
					return Redirect::route('teachers.index');
					break;

				case 'student':
					return Redirect::route('students.index');
					break;
				
				default:
					return App::abort(400);
					break;
			}
		}
		]);



