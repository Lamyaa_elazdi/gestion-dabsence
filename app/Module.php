<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Teacher;

class Module extends Model
{
	protected $fillable = ['entitled_module'];
	
    public function teachers(){

    	return $this->belongsToMany(Teacher::class,'module_teacher','module_id','teacher_id');
    }
}
