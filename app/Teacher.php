<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Module;

class Teacher extends Model
{
    protected $fillable=['fname','lname','adress','city','image','email','phone'];
    
    public function modules(){

    	return $this->belongsToMany(Module::class);
    }
}
