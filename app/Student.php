<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['CNE','fname','lname','adress','city','image','email','phone_number'];
}
