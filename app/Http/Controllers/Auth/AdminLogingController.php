<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLogingController extends Controller
{
    //setting up a middeleware

    public function __construct()
    {
    	$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
    	return view('auth.admin-login');
    }

    public function login(Request $request)
    {
    	//validate data
    	$this->validate($request, [
    		'email' => 'required|email',
			'password' => 'required|min:6'

    		]);

    	//attempt to log the user in 

    	if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
    	{
    		return redirect()->intended(route('admin.dashboard'));
    	}
    	//if successful , then redirect to there intended page

    	//if unsuccessful, then redirect back to the form login 
    	return redirect()->back()->withInput($request->only('email', 'remember'));
    }
     public function logout(Request $request)
    {
        $this->guard('admin')->logout();
         $request->session()->flush();

        $request->session()->regenerate();
         return redirect('/');
    }
protected function guard()
    {
        return Auth::guard();
    }
}
