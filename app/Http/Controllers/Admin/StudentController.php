<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use Session;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return view('Admin.students.allStudents')->withStudents($students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate input
        $this->validate($request,[

            'CNE'=>'required',
            'fname'=>'required|max:20',
            'lname'=>'required',
            'adress' => 'required',
            'city'=>'required|max:10',  
            'image' => 'required',
            'email'=>'required' ,   
            'phone'=>'required',
            'birth_date'=>'required|date',
            'birth_city'=>'required'

            ]);

        //Save the student as user 
         $user = new User;
         $user->who = "student";
         $user->name = $request->fname;
         $user->email = $request->email;
         $user->password = Hash::make('secret');
         $user->save();

        $student = new Student($request->input());
        $student->CNE = $request->CNE;
        $student->fname = $request->fname;
        $student->lname = $request->lname;
        $student->adress = $request->adress;
        $student->city = $request->city;
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->birth_date = $request->birth_date;
        $student->birth_city = $request->birth_city;
        if($file = $request->hasFile('image')){

            $file = $request->file('image');
            $fileName = getClientOriginalName();
            $destinationPath = public_path().'/images/'; 
            $file->move($destinationPath,$fileName);
            $student->image = $fileName;


         }
         $student->save();
         Session::flash('success','The student is successfully saved!!');
         return redirect()->route('students.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
