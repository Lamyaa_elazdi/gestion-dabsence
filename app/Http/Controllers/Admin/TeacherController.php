<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\file;
use Illuminate\Support\Facades\Input;
use App\Teacher;
use App\Module;
use App\User;
use Session;
use Hash;


class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //All teachers
        $teachers = Teacher::all();
        return view('Admin.teachers.AllTeachers')->withTeachers($teachers);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::all();
        return view('Admin.teachers.create')->withModules($modules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $this->validate($request,[

            'fname'=>'required|max:20',
            'lname'=>'required',
            'adress' => 'required',
            'city'=>'required|max:10',  
            'image' => 'required',
            'email'=>'required' ,   
            'phone'=>'required'   
        ]);

         //Save the teacher as user 
         $user = new User;
         $user->who = "teacher";
         $user->name = $request->fname;
         $user->email = $request->email;
         $user->password = Hash::make('secret');
         $user->save();

         //Save the teacher 

         $teacher = new Teacher($request->input());
         $teacher->fname = $request->fname;
         $teacher->lname = $request->lname;
         $teacher->adress = $request->adress;
         $teacher->city = $request->city;
         $teacher->email = $request->email;
         $teacher->phone = $request->phone;
         if($file = $request->hasFile('image')){
            $file = $request->file('image');
            $fileName = getClientOriginalName();
            $destinationPath = public_path().'/images/'; 
            $file->move($destinationPath,$fileName);
            $teacher->image = $fileName;


         }
        
         $teacher->save();
         $teacher->modules()->sync($request->modules, false);
         
       //   Session::flash('success','The teacher is successfully saved!!');
         return redirect()->route('teachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
             */
    public function show($id)
    {
        //find teacher
        $teacher = Teacher::find($id);
        return view('Admin.teachers.show',compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get teacher by id
        $teacher = Teacher::find($id);
        $modules = Module::all();
        return view('Admin.teachers.edit')->with('teacher',$teacher)->withModules($modules);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
          $this->validate($request,[

            'fname'=>'required|max:20',
            'lname'=>'required',
            'adress' => 'required',
            'city'=>'required|max:10',  
            'image' => 'required',
            'email'=>'required' ,   
            'phone'=>'required'   
        ]);

          $inputData = $request->all();
           //find the teacher updated by id
        $teacher = Teacher::find($id);
        $teacher->update($inputData);
        if (isset($request->modules)) {

            $teacher->modules()->sync($request->modules);
        }else{
            $teacher->modules()->sync(array());
        }

        return redirect()->route('teachers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find the teacher by id then delete him
        $teacher = Teacher::find($id);
        $teacher->delete();
        return redirect()->route('teachers.index');
    }
}
